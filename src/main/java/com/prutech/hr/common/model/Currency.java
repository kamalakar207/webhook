package com.prutech.hr.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Currency {
	
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int currencyId;
	private String currencyType;
	private String currencyName;
}
