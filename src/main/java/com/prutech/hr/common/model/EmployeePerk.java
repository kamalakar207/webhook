package com.prutech.hr.common.model;



import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeePerk {
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employeePerkId;
	private int component;
	private String componentType;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="currency_id")
	private Currency currency;
	
	@ManyToOne
	@JoinColumn(name="organization_id")
	private Organization organization;
}
