package com.prutech.hr.common.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Organization {
	
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int organizationId;
	private String organizationName;
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean isWeekendIncludedInLeaves;
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean isCompoffAvaliable;
	private int eligibilityExpirePeriod;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="organization_address_id")
	private Address organizationAddress;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="organization_calendar_id")
	private OrganizationCalendar organizationCalendar;
}
