package com.prutech.hr.common.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employeeId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String employeeNumber;
	private Date prohibitionCompletionDate;
	private String emailId;
	private String maritalStatus;
	private String fatherName;
	private String motherName;
	private String nomineeName;
	private String nomineeRelation;
	private String nomineeContactNumber;
	private String phoneNumber;
	private String alternativePhoneNumber;
	private Date dateOfBirth;
	private Date dateOfJoining;
	private String gender;

	private String emergencyContactName;
	private String emergencyContactNumber;
	private String emergencyContactRelation;
	private String remarks;
	
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean status;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="manager_id")
	private Employee manager;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="employment_type_id")
	private EmploymentType employmentType;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="nationality_id")
	private Nationality nationality;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="designation_id")
	private Designation designation;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="permanent_address_id")
	private Address permanentAddress;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="nominee_address_id")
	private Address nomineeAddress;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="present_address_id")
	private Address presentAddress;
}
