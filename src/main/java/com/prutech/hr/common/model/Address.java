package com.prutech.hr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
	
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int addressId;
	private String line1;
	private String line2;
	private String city;
	private String state;
	private String landmark;
	private String zipCode;
	private String geoCoordinates;
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean isPrimary;
}
