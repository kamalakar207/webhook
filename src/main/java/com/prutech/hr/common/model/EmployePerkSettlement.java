package com.prutech.hr.common.model;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployePerkSettlement {
	private String  uid;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employeePerkSettlementId;
	private int perkPaymentPeriod;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="employment_type_id")
	private EmploymentType employmentType;
}
