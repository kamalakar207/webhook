package com.prutech.hr.common.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Designation {

	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int designationId;
	private String designationName;
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean status;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="organization_id")
	private Organization organizationId;
}
