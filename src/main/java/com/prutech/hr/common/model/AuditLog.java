package com.prutech.hr.common.model;

import java.util.Date;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditLog {

	private Object object;
	private String newValue;
	private String oldValue;
	private String doneBy;
	private Date createdDate;
	
}
