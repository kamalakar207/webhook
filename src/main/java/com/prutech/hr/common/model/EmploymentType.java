package com.prutech.hr.common.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmploymentType {
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employmentHistoryId;
	private String employer;
	private Date fromDate;
	private Date toDate;
	private String designation;
	private double ctc;
}
