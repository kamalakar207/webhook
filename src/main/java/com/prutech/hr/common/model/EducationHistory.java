package com.prutech.hr.common.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EducationHistory {
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int educationHistoryId;
	private String educationInstitutionCity;
	private String educationInstitutionState;
	private String educationInstitutionCountry;
	private Date fromDate;
	private Date toDate;
	private String certification;
	private String percentage;
	private String certificationType;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name="employee_id")
	private Employee employee;
}
