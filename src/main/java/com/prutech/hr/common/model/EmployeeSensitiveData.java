package com.prutech.hr.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeSensitiveData {
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employeeSentiveDataId;
	private String panNumber;
	private String bankAccountNumber;
	private String institutionName;
	private String institutionType;
	private String accountNumber;
	private String passportNumber;
	
	@OneToOne
	@JoinColumn(name="employee_id")
	private Employee employee;
}
