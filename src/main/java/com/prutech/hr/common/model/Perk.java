package com.prutech.hr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Perk {
	
	private String uId;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int perkId;
	private String perkName;
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean isDeduction;
	@Column(columnDefinition = "TINYINT(1)", nullable = false)
	private boolean status;
}
